import { useState } from 'react';

export const useSelectedModule = () => {
  const [selectedModule, setSelectedModule] = useState(null);

  const handleModuleClick = (module) => {
    setSelectedModule(module);
  };

  return { selectedModule, handleModuleClick };
};