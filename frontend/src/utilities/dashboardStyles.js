export const dashboardTheme = {
  colors: {
    background: '#1c1c1e',
    text: '#ffffff',
    primary: '#0a84ff',
    secondary: '#30d158',
    tertiary: '#5e5ce6',
    quaternary: '#bf5af2',
    border: '#3a3a3c',
    hoverBackground: '#2c2c2e',
    selectedBackground: '#48484a'
  },
  fonts: {
    body: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif',
    heading: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif'
  },
  borderRadius: '12px',
  borderWidth: '1px',
  gridStyle: {
    templateColumns: { base: 1, md: 2, lg: 3 },
    spacing: 10,
    p: 10
  },
  logoStyle: {
    src: 'https://inkdrop.ai/wp-content/uploads/2022/03/cropped-Inkdrop_White-removebg-preview-300x54.png',
    alt: 'Inkdrop Logo'
  }
};

export const navbarStyle = {
  bg: dashboardTheme.colors.background,
  p: 4
};

export const moduleCardStyle = (isSelected) => ({
  cursor: 'pointer',
  borderWidth: dashboardTheme.borderWidth,
  borderRadius: dashboardTheme.borderRadius,
  bg: isSelected ? dashboardTheme.colors.selectedBackground : dashboardTheme.colors.background,
  _hover: {
    bg: dashboardTheme.colors.hoverBackground
  },
  p: 4
});

export const variablesOutputsContainerStyle = {
  align: 'stretch',
  spacing: 4,
  m: 4
};

export const sectionStyle = {
  p: 4,
  borderWidth: dashboardTheme.borderWidth,
  borderRadius: dashboardTheme.borderRadius
};

export const gridItemStyle = {
  p: 4,
  borderWidth: dashboardTheme.borderWidth,
  borderRadius: dashboardTheme.borderRadius
};