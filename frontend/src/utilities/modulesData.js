export const cloudResourceModules = [
  {
    id: 'module1',
    type: 'active',
    icon: 'FaDatabase',
    variables: [
      { name: 'var1', id: 'var1' },
      { name: 'var2', id: 'var2' },
      { name: 'var3', id: 'var3' }
    ],
    outputs: [
      { name: 'output1', id: 'output1' },
      { name: 'output2', id: 'output2' },
      { name: 'output3', id: 'output3' }
    ]
  },
  {
    id: 'module2',
    type: 'related',
    icon: 'FaDatabase',
    variables: [
      { name: 'var4', id: 'var4' },
      { name: 'var5', id: 'var5' },
      { name: 'var6', id: 'var6' }
    ],
    outputs: [
      { name: 'output4', id: 'output4' },
      { name: 'output5', id: 'output5' },
      { name: 'output6', id: 'output6' }
    ]
  }
  // ... other module objects
];