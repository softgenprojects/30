import React from 'react';
import { SimpleGrid, Box, VStack } from '@chakra-ui/react';
import { Navbar } from '@components/Navbar';
import { ModuleCard } from '@components/ModuleCard';
import { VariablesOutputsDisplay } from '@components/VariablesOutputsDisplay';
import { dashboardTheme } from '@utilities/dashboardStyles';
import { useSelectedModule } from '@hooks/useSelectedModule';
import { cloudResourceModules } from '@utilities/modulesData';

const Dashboard = () => {
  const { selectedModule, handleModuleClick } = useSelectedModule();

  return (
    <Box bg={dashboardTheme.colors.background} color={dashboardTheme.colors.text} minH='100vh'>
      <Navbar />
      <VStack spacing={8} align='stretch'>
        <SimpleGrid columns={dashboardTheme.gridStyle.templateColumns} spacing={dashboardTheme.gridStyle.spacing} p={dashboardTheme.gridStyle.p}>
          {cloudResourceModules.map((module) => (
            <Box key={module.id} onClick={() => handleModuleClick(module)}>
              <ModuleCard module={module} isSelected={selectedModule && module.id === selectedModule.id} />
            </Box>
          ))}
        </SimpleGrid>
        <VariablesOutputsDisplay selectedModule={selectedModule} />
      </VStack>
    </Box>
  );
};

export default Dashboard;