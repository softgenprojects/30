import React from 'react';
import { Box, Text, VStack, useColorModeValue, Icon } from '@chakra-ui/react';
import { FaDatabase } from 'react-icons/fa';
import { dashboardTheme } from '@utilities/dashboardStyles';

export const ModuleCard = ({ module }) => {
  const bgColor = useColorModeValue(dashboardTheme.colors.lightBackground, dashboardTheme.colors.darkBackground);
  const iconColor = module.type === 'active' ? dashboardTheme.colors.activeIcon : dashboardTheme.colors.inactiveIcon;
  const textColor = useColorModeValue(dashboardTheme.colors.darkText, dashboardTheme.colors.lightText);

  return (
    <VStack
      p={5}
      bg={bgColor}
      border='1px solid'
      borderColor={useColorModeValue('gray.200', 'gray.700')}
      borderRadius='lg'
      spacing={4}
      align='stretch'
      _hover={{ transform: 'translateY(-2px)', boxShadow: '0px 8px 24px rgba(0, 0, 0, 0.15)' }}
      transition='transform 0.2s, box-shadow 0.2s'
      role='group'
    >
      <Icon as={FaDatabase} w={8} h={8} color={iconColor} />
      <Text fontSize='xl' fontWeight='semibold' color={textColor}>
        {module.type === 'active' ? 'Active Resource' : 'Related Resource'}
      </Text>
      <Box as='ul' listStyleType='none' m={0} p={0}>
        {module.variables.map((variable, index) => (
          <Text as='li' key={variable.id} fontSize='md' color={textColor}>
            {variable.name}
          </Text>
        ))}
      </Box>
      <Box as='ul' listStyleType='none' m={0} p={0}>
        {module.outputs.map((output, index) => (
          <Text as='li' key={output.id} fontSize='md' color={textColor}>
            {output.name}
          </Text>
        ))}
      </Box>
    </VStack>
  );
};