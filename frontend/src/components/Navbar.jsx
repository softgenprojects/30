import { Flex, Image, Container } from '@chakra-ui/react';
import { dashboardTheme } from '@utilities/dashboardStyles';

export const Navbar = () => {
  return (
    <Container
      as='nav'
      maxW='container.xl'
      display='flex'
      alignItems='center'
      justifyContent='space-between'
      wrap='wrap'
      py={{ base: 2, md: 4 }}
      px={{ base: 4, md: 8 }}
      bg={dashboardTheme.colors.background}
      color={dashboardTheme.colors.text}
      boxShadow='sm'
      borderRadius={dashboardTheme.borderRadius}
    >
      <Flex align='center' mr={5}>
        <Image
          src={dashboardTheme.logoStyle.src}
          alt={dashboardTheme.logoStyle.alt}
          htmlWidth='auto'
          htmlHeight='auto'
        />
      </Flex>
      {/* Add additional Navbar items here */}
    </Container>
  );
};