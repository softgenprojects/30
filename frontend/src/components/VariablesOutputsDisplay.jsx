import React from 'react';
import { Box, Grid, Text, VStack, Heading, GridItem } from '@chakra-ui/react';
import { dashboardTheme } from '@utilities/dashboardStyles';

export const VariablesOutputsDisplay = ({ selectedModule }) => {
  const borderColor = dashboardTheme.colors.border;
  const bg = dashboardTheme.colors.background;
  const highlightBg = dashboardTheme.colors.selectedBackground;
  const textColor = dashboardTheme.colors.text;

  if (!selectedModule) {
    return (
      <Box p={4} borderWidth={dashboardTheme.borderWidth} borderColor={borderColor} borderRadius={dashboardTheme.borderRadius} bg={bg} color={textColor}>
        <Text>Select a Module to see Variables and Outputs.</Text>
      </Box>
    );
  }

  return (
    <VStack align='stretch' spacing={4} m={4}>
      <Box p={4} borderWidth={dashboardTheme.borderWidth} borderColor={borderColor} borderRadius={dashboardTheme.borderRadius} bg={highlightBg} color={textColor}>
        <Heading size='md' color={textColor}>Variables</Heading>
        <Grid templateColumns='repeat(3, 1fr)' gap={6}>
          {selectedModule.variables.map((variable, index) => (
            <GridItem key={variable.id} p={4} borderWidth={dashboardTheme.borderWidth} borderColor={borderColor} borderRadius={dashboardTheme.borderRadius} bg={bg} color={textColor}>
              {variable.name}
            </GridItem>
          ))}
        </Grid>
      </Box>
      <Box p={4} borderWidth={dashboardTheme.borderWidth} borderColor={borderColor} borderRadius={dashboardTheme.borderRadius} bg={highlightBg} color={textColor}>
        <Heading size='md' color={textColor}>Outputs</Heading>
        <Grid templateColumns='repeat(3, 1fr)' gap={6}>
          {selectedModule.outputs.map((output, index) => (
            <GridItem key={output.id} p={4} borderWidth={dashboardTheme.borderWidth} borderColor={borderColor} borderRadius={dashboardTheme.borderRadius} bg={bg} color={textColor}>
              {output.name}
            </GridItem>
          ))}
        </Grid>
      </Box>
    </VStack>
  );
};